DROP  TABLE IF EXISTS `dispatcher`;
CREATE TABLE dispatcher (
  `id`                  BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `name`                VARCHAR(64),
  `mailing_address`     VARCHAR(300),
  `created_by`          VARCHAR(64),
  `status`              VARCHAR(20),
  `is_active`             BIT(1)       NOT NULL DEFAULT b'1',
  `record_version_number` INT(4) DEFAULT NULL,
  `created_timestamp`   bigint(15),
  `updated_timestamp`   bigint(15),
   PRIMARY KEY (`id`)
)ENGINE = InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8;

DROP  TABLE IF EXISTS `invoice_dispatcher`;
CREATE TABLE invoice_dispatcher (
  `id`                BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `invoice_number`    VARCHAR(128)   NOT NULL,
  `dispatch_id`       BIGINT(20)   NOT NULL,
  `is_active`             BIT(1)   NOT NULL DEFAULT b'1',
  `record_version_number` INT(4) DEFAULT NULL,
  `created_timestamp`   bigint(15),
  `updated_timestamp`   bigint(15),
   PRIMARY KEY (`id`),
   FOREIGN KEY `invoice_dispatcher_dispatch_id_fk` (`dispatch_id`) REFERENCES `dispatcher` (`id`)
)ENGINE = InnoDB  DEFAULT CHARSET = utf8;

create index invoice_dispatcher_key
  on invoice_dispatcher (dispatch_id);
