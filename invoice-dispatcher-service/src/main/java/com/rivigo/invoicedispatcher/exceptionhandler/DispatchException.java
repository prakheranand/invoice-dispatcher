package com.rivigo.invoicedispatcher.exceptionhandler;

import lombok.Data;

@Data
public class DispatchException extends RuntimeException{
    private Integer appErrorCode;
    private String appErrorMessage;

    public DispatchException(Integer appErrorCode, String appErrorMessage) {
        super(appErrorMessage);
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public DispatchException(String appErrorMessage) {
        super(appErrorMessage);
        this.appErrorMessage = appErrorMessage;
    }

}
