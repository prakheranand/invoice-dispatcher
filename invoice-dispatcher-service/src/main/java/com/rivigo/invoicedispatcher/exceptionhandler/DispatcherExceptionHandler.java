package com.rivigo.invoicedispatcher.exceptionhandler;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.rivigo.finance.exceptions.FinanceException;
import com.rivigo.finance.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.PersistenceException;

@Component
@Slf4j
@ControllerAdvice
public class DispatcherExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = { DispatchException.class , FinanceException.class })
    protected ResponseEntity<Response> handleDispatchException(RuntimeException ex,
                                                          WebRequest request) {
        log.error("DispatchException occoured - {}", ex);

        DispatchException DispatchException;
        FinanceException financeException;
        if (ex instanceof DispatchException) {
            DispatchException = (DispatchException) ex;
            return new ResponseEntity<>(
                    buildResponse(DispatchException.getAppErrorCode(), DispatchException.getAppErrorMessage()),
                    HttpStatus.BAD_REQUEST);
        }
        else {
            financeException = (FinanceException) ex;
            return new ResponseEntity<>(
                    buildResponse(financeException.getAppErrorCode(), financeException.getAppErrorMessage()),
                    HttpStatus.BAD_REQUEST);
        }

    }


    @ExceptionHandler(value = {AccessDeniedException.class })
    protected ResponseEntity<Response> handleAccessDeniedException(RuntimeException ex,
                                                                   WebRequest request) {
        log.error("DispatchException occoured - {}", ex);
        AccessDeniedException e = (AccessDeniedException) ex;

        return new ResponseEntity<>(
                buildResponse(401, e.getMessage()),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = { PersistenceException.class })
    protected ResponseEntity<Response> handleSqlException(RuntimeException ex,
                                                          WebRequest request) {
        String message = null;

        // This is to handle the unique constraint errors while persisting entities.
        // We do not check if entities persisted are unique.
        // We rely on the MySQL Constraint Exception to ensure uniqueness.
        if (ex.getCause() != null
                && ex.getCause().getCause() != null
                && ex.getCause()
                .getCause() instanceof MySQLIntegrityConstraintViolationException) {

            MySQLIntegrityConstraintViolationException exception = (MySQLIntegrityConstraintViolationException) ex
                    .getCause().getCause();
            message = exception.getMessage();
        }
        else {
            log.error("Sql Exception occoured - {}", ex);
            message = "Failed to save";
        }

        return new ResponseEntity<>(buildResponse(500, message),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = { Exception.class })
    protected ResponseEntity<Response> handleUnknownException(RuntimeException ex,
                                                              WebRequest request) {
        log.error("Exception occoured - {}", ex);
        return new ResponseEntity<>(buildResponse(500, "Something went wrong!"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private Response buildResponse(Integer errorCode, String errorMessage) {
        return Response.builder().appErrorCode(errorCode).errorMessage(errorMessage)
                .build();
    }

}
