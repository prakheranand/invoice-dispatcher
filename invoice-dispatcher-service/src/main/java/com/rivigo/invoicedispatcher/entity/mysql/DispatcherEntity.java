package com.rivigo.invoicedispatcher.entity.mysql;

import com.rivigo.finance.entity.mysql.base.BaseAuditableEntity;
import com.rivigo.finance.entity.mysql.base.BaseEntity;
import lombok.*;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Data
@Table(name = "dispatcher")
public class DispatcherEntity{
    private static final long serialVersionUID = 2988243687777035575L;

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(name = "id", length = 128)
    private Long id;

    @Column(name = "name", length = 128)
    private String name;

    @Column(name = "mailing_address")
    private String mailingAddress;

    @Column(name = "status")
    private String status;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_active")
    private Boolean isActive = true;

    @Column(name = "record_version_number")
    private Integer recordversion = 1;

    @PrePersist
    public void onCreate() {
        this.updatedTimestamp = this.createdTimestamp = (DateTime.now().getMillis()/1000);
        this.isActive = null == this.isActive ? true : this.isActive;
    }

    @PreUpdate
    public void onUpdate() {
        this.updatedTimestamp = (DateTime.now().getMillis()/1000);
    }

}