package com.rivigo.invoicedispatcher.entity.mysql;

import com.rivigo.finance.entity.mysql.base.BaseAuditableEntity;
import com.rivigo.finance.entity.mysql.base.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "invoice")
public class InvoicingEntity{
    private static final long serialVersionUID = 2988243687777035575L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "invoice_number")
    private String invoiceNumber;

    @Column(name = "client_code")
    private String clientCode;

    @Column(name = "annexure_pdf_url")
    private String annexurePdf;

    @Column(name = "url")
    private String invoicePdf;

    @Column(name = "zipped_url")
    private String pod;

    @Column(name = "client_address")
    private String mailingAddressHash;

}
