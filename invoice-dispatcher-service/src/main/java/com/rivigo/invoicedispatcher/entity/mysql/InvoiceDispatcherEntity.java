package com.rivigo.invoicedispatcher.entity.mysql;

import com.rivigo.finance.entity.mysql.base.BaseEntity;
import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Data
@Table(name = "invoice_dispatcher")
public class InvoiceDispatcherEntity {
    private static final long serialVersionUID = 2988243687777035575L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(name = "id", length = 128)
    private Long id;

    @Column(name = "invoice_number", nullable = false)
    private String invoiceNumber;

    @Column(name = "dispatch_id", nullable = false, length = 128)
    private Long dispatchId;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_active")
    private Boolean isActive = true;

    @Column(name = "record_version_number")
    private Integer recordversion = 1;

    @PrePersist
    public void onCreate() {
        this.updatedTimestamp = this.createdTimestamp = (DateTime.now().getMillis()/1000);
        this.isActive = null == this.isActive ? true : this.isActive;
    }

    @PreUpdate
    public void onUpdate() {
        this.updatedTimestamp = (DateTime.now().getMillis()/1000);
    }
}
