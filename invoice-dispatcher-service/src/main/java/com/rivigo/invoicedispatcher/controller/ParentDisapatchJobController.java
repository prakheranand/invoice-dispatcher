package com.rivigo.invoicedispatcher.controller;

import com.rivigo.finance.response.Response;
import com.rivigo.invoicedispatcher.api.ParentDispatcherJobDto;
import com.rivigo.invoicedispatcher.api.PrintJobDto;
import com.rivigo.invoicedispatcher.entity.mysql.DispatcherEntity;
import com.rivigo.invoicedispatcher.service.ParentDispatchJobService;
import io.swagger.annotations.ApiImplicitParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/parentdispatch")
public class ParentDisapatchJobController {

    @Autowired
    private ParentDispatchJobService parentDispatchJobService;

    //dummy controller for testing
    @RequestMapping(value = "", method = RequestMethod.POST)
  //  @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<List<DispatcherEntity>> createDispatchJobs(@RequestBody ParentDispatcherJobDto pdto) {
      log.debug("parent dispatch task received {} ", pdto);
      List<DispatcherEntity> dispatcherEntityList = parentDispatchJobService.createDispatchJobs(pdto);
      return new Response<>(dispatcherEntityList);
    }
}
