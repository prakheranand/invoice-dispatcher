package com.rivigo.invoicedispatcher.controller;

import com.rivigo.finance.response.Response;
import com.rivigo.invoicedispatcher.api.DispatchJobDto;
import com.rivigo.invoicedispatcher.api.InvoiceDetailsDto;
import com.rivigo.invoicedispatcher.service.DispatchJobservice;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/dispatch")
public class DispatchJobController {
    private static final String AUTHORIZATION_KEY = "Authorization";

    @Autowired
    private DispatchJobservice dispatchJobservice;

    @RequestMapping(value = "", method = RequestMethod.GET)
 //   @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<List<DispatchJobDto>> get() {
        return new Response<>(dispatchJobservice.getAllDispatchJob("approved"));
    }

    @RequestMapping(value = "/getone", method = RequestMethod.GET)
    //   @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<DispatchJobDto> getone() {
        return new Response<>(dispatchJobservice.getOne());
    }

    //TODO : return list of invoices and related details if any
    @RequestMapping(value = "{dispatchId}", method = RequestMethod.GET)
   // @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<DispatchJobDto> getInvoiceIdsFromDispatchId
            (@PathVariable("dispatchId") Long dispatchId) {
        return new Response<>(dispatchJobservice.getInvoiceIdsFromDispatchId(dispatchId));

    }

}
