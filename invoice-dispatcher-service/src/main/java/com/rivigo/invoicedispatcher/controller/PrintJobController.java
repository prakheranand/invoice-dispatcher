package com.rivigo.invoicedispatcher.controller;

import com.rivigo.finance.response.Response;
import com.rivigo.invoicedispatcher.api.CdBurnJobDto;
import com.rivigo.invoicedispatcher.api.DispatchJobDto;
import com.rivigo.invoicedispatcher.api.LabelPrintJobDto;
import com.rivigo.invoicedispatcher.api.PrintJobDto;
import com.rivigo.invoicedispatcher.service.PrintJobService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/print")
public class PrintJobController {

    private static final String AUTHORIZATION_KEY = "Authorization";

    @Autowired
    private PrintJobService printJobService;

    @RequestMapping(value = "/pdf", method = RequestMethod.POST)
  //  @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<String> createPrintPdfJobs(@RequestBody List<PrintJobDto> printJobsDto) {
        printJobsDto.stream().forEach(x -> printJobService.putPrintJobIntoQueue(x));
        return new Response<>("Ok");
    }

    @RequestMapping(value = "/cd", method = RequestMethod.POST)
    //  @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<String> createCdBurnJobs(@RequestBody List<CdBurnJobDto> printJobsDto) {
        printJobsDto.stream().forEach(x -> printJobService.putCdJobIntoQueue(x));
        return new Response<>("Ok");
    }

    @RequestMapping(value = "/label", method = RequestMethod.POST)
    //  @ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<String> createPrintJobs(@RequestBody List<LabelPrintJobDto> printJobsDto) {
        printJobsDto.stream().forEach(x -> printJobService.putLabelPrintJobIntoQueue(x));
        return new Response<>("Ok");
    }

    @RequestMapping(value = "/pdf", method = RequestMethod.GET)
    //@ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<PrintJobDto> getPrintPdfJobs() {
        return new Response<>(printJobService.getPrintJob());
    }
    @RequestMapping(value = "/cd", method = RequestMethod.GET)
    //@ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<CdBurnJobDto> getCdBurnJobs() {
        return new Response<>(printJobService.getCdBurnJob());
    }
    @RequestMapping(value = "/label", method = RequestMethod.GET)
    //@ApiImplicitParam(name = AUTHORIZATION_KEY, value = "Authorization Key", required = true, dataType = "string", paramType = "header")
    public Response<LabelPrintJobDto> getLabelPrintJobs() {
        return new Response<>(printJobService.getLabelPrintJob());
    }

}
