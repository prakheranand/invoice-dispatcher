package com.rivigo.invoicedispatcher.event;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rivigo.invoicedispatcher.api.ParentDispatcherJobDto;
import com.rivigo.invoicedispatcher.interfaces.ParentDispatchJobConsumer;
import com.rivigo.invoicedispatcher.interfaces.ParentDispatchJobServiceInterface;
import com.rivigo.invoicedispatcher.service.ParentDispatchJobService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class ParentDispatchKafkaConsumer implements ParentDispatchJobConsumer {

    @Autowired
    @Qualifier("ParentDispatchJobService")
    private ParentDispatchJobService parentDispatchJobService;

    @Override
    @KafkaListener(topics = {"${parent.dispatch.job.topic}"}, groupId = "${group.id.config}")
    public void consumes(ConsumerRecord<?, ?> record) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        TypeReference<ParentDispatcherJobDto> mapType = new TypeReference<ParentDispatcherJobDto>() {};
        ParentDispatcherJobDto parentDispatcherJobDto = objectMapper.readValue(record.value().toString(), mapType);
        parentDispatchJobService.createDispatchJobs(parentDispatcherJobDto);
    }
}
