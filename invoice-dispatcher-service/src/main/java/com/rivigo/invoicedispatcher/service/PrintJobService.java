package com.rivigo.invoicedispatcher.service;

import com.rivigo.invoicedispatcher.api.CdBurnJobDto;
import com.rivigo.invoicedispatcher.api.LabelPrintJobDto;
import com.rivigo.invoicedispatcher.api.PrintJobDto;
import org.springframework.stereotype.Service;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service(value = "PrintJobService")
public class PrintJobService {
    private static final String defaultQueue = "deafult";
    private static final String priorityQueue = "high";

    /*
        TODO : we must move these two queue to hazelcast. Once a printjobs submitted to printutils, create
        a task in mongo. If task is not marked dispatched after certain time , reinsert that printjob to queue again.
        state : approved -> inPrinting -> dispatched.
        Any dispatch job can move to approved state (for reprinting).
     */
    private Queue<PrintJobDto> printNormalQueue = new ConcurrentLinkedQueue<>();
    private Queue<PrintJobDto> printHighQeueu = new ConcurrentLinkedQueue<>();

    private Queue<CdBurnJobDto> cdNormalQueue = new ConcurrentLinkedQueue<>();
    private Queue<CdBurnJobDto> cdHighQeueu = new ConcurrentLinkedQueue<>();

    private Queue<LabelPrintJobDto> labelNormalQueue = new ConcurrentLinkedQueue<>();
    private Queue<LabelPrintJobDto> labelHighQeueu = new ConcurrentLinkedQueue<>();

    public PrintJobDto putPrintJobIntoQueue(PrintJobDto printJobDto) {
        if(priorityQueue.equalsIgnoreCase(printJobDto.getPriority())){
            printHighQeueu.add(printJobDto);
        }
        else {
            printNormalQueue.add(printJobDto);
        }
        return printJobDto;
    }

    public PrintJobDto getPrintJob() {
        if(!printHighQeueu.isEmpty()){
            return printHighQeueu.remove();
        }
        else if(!printNormalQueue.isEmpty()){
            return printNormalQueue.remove();
        }
        return null;
    }
    public CdBurnJobDto putCdJobIntoQueue(CdBurnJobDto cdBurnJobDto) {
        if(priorityQueue.equalsIgnoreCase(cdBurnJobDto.getPriority())){
            cdHighQeueu.add(cdBurnJobDto);
        }
        else {
            cdNormalQueue.add(cdBurnJobDto);
        }
        return cdBurnJobDto;
    }

    public CdBurnJobDto getCdBurnJob() {
        if(!cdHighQeueu.isEmpty()){
            return cdHighQeueu.remove();
        }
        else if(!cdNormalQueue.isEmpty()){
            return cdNormalQueue.remove();
        }
        return null;
    }
    public LabelPrintJobDto putLabelPrintJobIntoQueue(LabelPrintJobDto labelPrintJobDto) {
        if(priorityQueue.equalsIgnoreCase(labelPrintJobDto.getPriority())){
            labelHighQeueu.add(labelPrintJobDto);
        }
        else {
            labelNormalQueue.add(labelPrintJobDto);
        }
        return labelPrintJobDto;
    }

    public LabelPrintJobDto getLabelPrintJob() {
        if(!labelHighQeueu.isEmpty()){
            return labelHighQeueu.remove();
        }
        else if(!labelNormalQueue.isEmpty()){
            return labelNormalQueue.remove();
        }
        return null;
    }

}
