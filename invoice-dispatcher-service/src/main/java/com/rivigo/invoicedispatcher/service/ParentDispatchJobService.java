package com.rivigo.invoicedispatcher.service;

import com.google.gson.Gson;
import com.rivigo.invoicedispatcher.api.DispatchJobDto;
import com.rivigo.invoicedispatcher.api.ParentDispatcherJobDto;
import com.rivigo.invoicedispatcher.entity.mysql.DispatcherEntity;
import com.rivigo.invoicedispatcher.entity.mysql.InvoiceDispatcherEntity;
import com.rivigo.invoicedispatcher.entity.mysql.InvoicingEntity;
import com.rivigo.invoicedispatcher.interfaces.ParentDispatchJobServiceInterface;
import com.rivigo.invoicedispatcher.repository.mysql.DispatcherRepository;
import com.rivigo.invoicedispatcher.repository.mysql.InvoiceDispatcherRepository;
import com.rivigo.invoicedispatcher.repository.mysql.InvoiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service(value = "ParentDispatchJobService")
public class ParentDispatchJobService implements ParentDispatchJobServiceInterface {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoiceDispatcherRepository invoiceDispatcherRepo;

    @Autowired
    private DispatcherRepository dispatcherRepository;

    @Autowired
    private DispatchJobservice dispatchJobservice;

    @Override
    public List<DispatcherEntity> createDispatchJobs(ParentDispatcherJobDto parentDispatcherJobDto) {
        List<InvoicingEntity> invoicingEntityList =
                invoiceRepository.findByInvoiceNumbers(parentDispatcherJobDto.getInvoiceNumbers());
        //group by mailing address
        Map<String, List<InvoicingEntity>> addressMap = new HashMap<>();
        for(InvoicingEntity invoicingEntity : invoicingEntityList) {
            List<InvoicingEntity> addressList = addressMap.get(invoicingEntity.getMailingAddressHash());
            if(addressList == null) {
                addressList = new ArrayList<>();
                addressMap.put(invoicingEntity.getMailingAddressHash(), addressList);
            }
            addressList.add(invoicingEntity);
        }
        return persistDispatchJob(addressMap);
    }

    @Transactional
    public List<DispatcherEntity> persistDispatchJob(Map<String, List<InvoicingEntity>> addressMap) {
        List<DispatcherEntity> dispatcherEntities = new ArrayList<>();
        for(Map.Entry<String, List<InvoicingEntity>> entry : addressMap.entrySet()) {
            DispatcherEntity dispatcherEntity = new DispatcherEntity();
            dispatcherEntity.setName(entry.getValue().get(0).getClientCode());
            dispatcherEntity.setMailingAddress(entry.getKey());
            dispatcherEntity.setStatus("approved");
            log.info("dispatcher entity to be saved {} ", new Gson().toJson(dispatcherEntity));
            DispatcherEntity dispatcherEntitySaved = dispatcherRepository.save(dispatcherEntity);
            dispatcherEntities.add(dispatcherEntitySaved);
            List<InvoiceDispatcherEntity> entities = new ArrayList<>();
            for(InvoicingEntity invoicingEntity : entry.getValue()) {
                InvoiceDispatcherEntity invoiceDispatcherEntity = new InvoiceDispatcherEntity();
                invoiceDispatcherEntity.setInvoiceNumber(invoicingEntity.getInvoiceNumber());
                invoiceDispatcherEntity.setDispatchId(dispatcherEntitySaved.getId());

                entities.add(invoiceDispatcherEntity);
            }
            invoiceDispatcherRepo.saveAll(entities);
        }
        dispatchJobservice.putIntoQueueForCaching(dispatcherEntities);
        return dispatcherEntities;
    }


}
