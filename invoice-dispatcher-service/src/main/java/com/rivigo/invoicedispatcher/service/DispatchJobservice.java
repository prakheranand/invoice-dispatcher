package com.rivigo.invoicedispatcher.service;

import com.rivigo.invoicedispatcher.api.DispatchJobDto;
import com.rivigo.invoicedispatcher.entity.mysql.DispatcherEntity;
import com.rivigo.invoicedispatcher.entity.mysql.InvoiceDispatcherEntity;
import com.rivigo.invoicedispatcher.exceptionhandler.DispatchException;
import com.rivigo.invoicedispatcher.repository.mysql.DispatcherRepository;
import com.rivigo.invoicedispatcher.repository.mysql.InvoiceDispatcherRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service(value = "DispatchJobservice")
public class DispatchJobservice {

    @Autowired
    private DispatcherRepository dispatcherRepository;

    @Autowired
    private InvoiceDispatcherRepository invoiceDispatcherRepository;

    /*
    TODO : this should must moved to hazelcast distributed queue.
     */
    private ConcurrentLinkedQueue<DispatchJobDto> dispatchJobDtoQueue = new ConcurrentLinkedQueue<>();

    public void putIntoQueueForCaching(List<DispatcherEntity> dispatcherEntities){
        dispatcherEntities.stream().forEach(x -> dispatchJobDtoQueue.add(convertToDispatchDto(x)));
    }

    public DispatchJobDto getOne() {
        if(!dispatchJobDtoQueue.isEmpty()){
            return dispatchJobDtoQueue.peek();
        }
        return null;
    }

    public List<DispatchJobDto> getAllDispatchJob(String status) {
        List<DispatcherEntity> dispatcherEntities = dispatcherRepository.findByStatus(status);
        List<DispatchJobDto> dispatchJobDtoList = new ArrayList<>();
        dispatcherEntities.stream().forEach( x -> dispatchJobDtoList.add(convertToDispatchDto(x)));
        return dispatchJobDtoList;
    }

    public DispatchJobDto convertToDispatchDto(DispatcherEntity dispatcherEntity){
        DispatchJobDto dispatchJobDto = new DispatchJobDto();
        dispatchJobDto.setDispatchId(dispatcherEntity.getId());
        dispatchJobDto.setName(dispatcherEntity.getName());
        List<InvoiceDispatcherEntity> invoiceDispatcherEntities =
                invoiceDispatcherRepository.findByDispatchId(dispatcherEntity.getId());
        List<String> invoiceNumbers = new ArrayList<>();
        invoiceDispatcherEntities.forEach(x -> invoiceNumbers.add(x.getInvoiceNumber()));
        dispatchJobDto.setInvoiceNumbers(invoiceNumbers);
        return dispatchJobDto;
    }

    public DispatchJobDto getInvoiceIdsFromDispatchId(Long dispatchId){
        Optional<DispatcherEntity> dispatcherEntityOptional = dispatcherRepository.findById(dispatchId);
        if(!dispatcherEntityOptional.isPresent()){
            throw new DispatchException("dispatch id invalid");
        }
        DispatcherEntity dispatcherEntity = dispatcherEntityOptional.get();
        DispatchJobDto dispatchJobDto = new DispatchJobDto();
        dispatchJobDto.setDispatchId(dispatcherEntity.getId());
        dispatchJobDto.setName(dispatcherEntity.getName());
        List<InvoiceDispatcherEntity> invoiceDispatcherEntities =
                invoiceDispatcherRepository.findByDispatchId(dispatchId);
        List<String> invoiceNumbers = new ArrayList<>();
        invoiceDispatcherEntities.forEach(x -> invoiceNumbers.add(x.getInvoiceNumber()));
        dispatchJobDto.setInvoiceNumbers(invoiceNumbers);
        return dispatchJobDto;
    }


}
