package com.rivigo.invoicedispatcher.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.AbstractMessageListenerContainer;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@EnableKafka
@Slf4j
public class KafkaConsumerConfig {

    private static Set<String> defaulters = new HashSet<>(0);

    @Value("${consumer.concurrency.level}")
    private int consumerConcurrencyLevel;

    @Value("${consumer.poll.timeout.millis}")
    private int consumerPollTimeoutMillis;

    @Value("${bootstrap.servers.config}")
    private String bootstrapServersConfig;

    @Value("${enable.auto.commit.config}")
    private Boolean enableAutoCommitConfig;

    @Value("${auto.commit.interval.ms.config}")
    private int autoCommitIntervalMillis;

    @Value("${session.timeout.ms.config}")
    private int sessionTimeoutMillis;

    @Value("${group.id.config}")
    private String consumerGroupId;

    @Value("${auto.offset.reset.config}")
    private String autoOffsetReset;

    @Value("${max.poll.interval.ms.config}")
    private int maxPollInterval;


    @Bean("kafkaListenerContainerFactory")
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
        log.info("Registering Kafka Listener Container Factory....");
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setConcurrency(consumerConcurrencyLevel);
        factory.getContainerProperties().setPollTimeout(consumerPollTimeoutMillis);
        factory.getContainerProperties().setAckMode(AbstractMessageListenerContainer.AckMode.RECORD);
        factory.setRetryTemplate(getRetryTemplate());
        return factory;
    }

    @Bean
    public RetryTemplate getRetryTemplate() {
        RetryTemplate rt = new RetryTemplate();
        SimpleRetryPolicy srp = new SimpleRetryPolicy();
        srp.setMaxAttempts(1);
        rt.setRetryPolicy(srp);
        return rt;
    }

    @Bean(name = "consumerFactory")
    public ConsumerFactory<String, String> consumerFactory() {
        log.info("************* Registering Consumer Factory ***************");
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> propsMap = new HashMap<>();
        propsMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
        propsMap.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        propsMap.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, autoCommitIntervalMillis);
        propsMap.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeoutMillis);
        propsMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        propsMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        propsMap.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId);
        propsMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        propsMap.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, maxPollInterval);
        propsMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1");
        return propsMap;
    }

}