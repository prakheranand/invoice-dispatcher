package com.rivigo.invoicedispatcher.config;

import com.rivigo.oauth2.resource.config.UserAccessTokenConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableResourceServer
@PropertySource(
        value = {"classpath:${login.profiles.active:staging}/authresource.properties",
                "classpath:${spring.profiles.active:dev}/login.properties",
                "classpath:${spring.profiles.active:local}/login.properties"},
        ignoreResourceNotFound = true
)
@ComponentScan({"com.rivigo.oauth2.resource.controller",
        "com.rivigo.oauth2.resource.model",
        "com.rivigo.oauth2.resource.service"})

public class DispatcherOAuth2ResourceConfig extends ResourceServerConfigurerAdapter {
    private TokenExtractor tokenExtractor = new BearerTokenExtractor();

    @Value("${open.access.url.patterns}")
    private String openAccessUrlPatterns;

    public DispatcherOAuth2ResourceConfig() {
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    public void configure(HttpSecurity http) throws Exception {
        http.addFilterAfter(new OncePerRequestFilter() {
            protected void doFilterInternal(HttpServletRequest request,
                                            HttpServletResponse response, FilterChain filterChain)
                    throws ServletException, IOException {
                if (tokenExtractor.extract(request) == null) {
                    SecurityContextHolder.clearContext();
                }

                filterChain.doFilter(request, response);
            }
        }, AbstractPreAuthenticatedProcessingFilter.class);
        List<String> openAccessUrlAntMatchers = new ArrayList();
        openAccessUrlAntMatchers.add("/create-user");
        openAccessUrlAntMatchers.add("/update-user**");
        openAccessUrlAntMatchers.add("/check-user**");
        openAccessUrlAntMatchers.add("/user-permission**");
        openAccessUrlAntMatchers.add("/access-token**");
        openAccessUrlAntMatchers.add("/check-token**");
        openAccessUrlAntMatchers.add("/revoke-token**");
        openAccessUrlAntMatchers.add("/dispatch**");
        openAccessUrlAntMatchers.add("/dispatch/**");
        openAccessUrlAntMatchers.add("/parentdispatch**");
        openAccessUrlAntMatchers.add("/print**");
        openAccessUrlAntMatchers.add("/print/**");
        openAccessUrlAntMatchers.add("/swagger**");
        openAccessUrlAntMatchers.add("/swagger-resources/**");

        if (this.openAccessUrlPatterns != null) {
            String[] var3 = this.openAccessUrlPatterns.split(",");
            int var4 = var3.length;

            for (int var5 = 0; var5 < var4; ++var5) {
                String pattern = var3[var5];
                if (!pattern.trim().isEmpty()) {
                    openAccessUrlAntMatchers.add(pattern.trim());
                }
            }
        }

        ((HttpSecurity) ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)
                ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)
                        ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)
                                http.authorizeRequests().antMatchers(HttpMethod.OPTIONS,
                                        new String[]{"/**"})).permitAll().antMatchers((String[])
                                openAccessUrlAntMatchers.toArray(new String[openAccessUrlAntMatchers.size()])))
                        .permitAll().anyRequest()).authenticated().and()).logout().logoutSuccessUrl("/").permitAll();
    }

    @Bean
    public AccessTokenConverter accessTokenConverter() {
        return new UserAccessTokenConverter();
    }

    @Bean
    public RemoteTokenServices remoteTokenServices(@Value("${auth.server.url}") String checkTokenUrl, @Value("${auth.server.clientId}") String clientId, @Value("${auth.server.clientsecret}") String clientSecret) {
        RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(checkTokenUrl);
        remoteTokenServices.setClientId(clientId);
        remoteTokenServices.setClientSecret(clientSecret);
        remoteTokenServices.setAccessTokenConverter(this.accessTokenConverter());
        return remoteTokenServices;
    }

    @Bean(
            name = {"myProperties"}
    )
    public PropertiesFactoryBean mapper(@Value("${login.profiles.active:staging}") String classPath) {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource(classPath + "/authresource.properties"));
        return bean;
    }

}
