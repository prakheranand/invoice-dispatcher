package com.rivigo.invoicedispatcher.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.Ordered;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PropertySourceConfig {
    private static final String externalPropResource = "external.property.resource";

    @Bean
    static PropertySourcesPlaceholderConfigurer configurer() throws IOException {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        String profile = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
        profile = (null == profile ? "dev" : profile);
        Resource[] resources = patternResolver.getResources(String.format("classpath*:%s/*.properties", profile));

        List<Resource> externalizedResourcesList = new ArrayList<>();
        // for prod n staging
        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        String externalPropResourceDir = runtimeMxBean.getSystemProperties()
                .get(externalPropResource);
        Resource[] resources2 = patternResolver.getResources("file:"+ externalPropResourceDir + "*.properties");
        for (Resource resource : resources2) {
            externalizedResourcesList.add(resource);
        }

        externalizedResourcesList.addAll(Arrays.asList(resources));
        Resource [] allResources = externalizedResourcesList.toArray(new Resource[externalizedResourcesList.size()]);
        configurer.setLocations(allResources);
        configurer.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return configurer;
    }

}
