package com.rivigo.invoicedispatcher.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
@Slf4j
public class KafkaProducerConfig {

    @Value("${bootstrap.servers.config}")
    private String bootstrapServersConfig;

    @Value("${retries.config}")
    private int retriesConfig;

    @Value("${max.in.flight.requests.per.connection}")
    private int maxInFlightRequestsPerConnection;

    @Value("${batch.size.config}")
    private int batchSizeConfig;

    @Value("${linger.ms.config}")
    private int lingerMsConfig;

    @Value("${buffer.memory.config}")
    private int bufferMemoryConfig;

    @Value("${request.timeout.ms.config}")
    private int requestTimeoutMsConfig;

    @Value("${max.block.ms.config}")
    private int maxBlockMsConfig;

    @Value("${ack.config}")
    private String ackConfig;

    @Value("${enable.idempotence}")
    private Boolean enableIdempotence;

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }

    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put("log4j.logger.kafka", "ERROR");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServersConfig);
        props.put(ProducerConfig.RETRIES_CONFIG, retriesConfig);
        props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, enableIdempotence);
        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, maxInFlightRequestsPerConnection);
        props.put(ProducerConfig.ACKS_CONFIG, ackConfig);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, batchSizeConfig);
        props.put(ProducerConfig.LINGER_MS_CONFIG, lingerMsConfig);
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeoutMsConfig);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, bufferMemoryConfig);
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, maxBlockMsConfig);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        log.info("Registering Kafka Template....");
        return new KafkaTemplate<>(producerFactory());
    }
}
