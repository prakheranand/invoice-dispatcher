package com.rivigo.invoicedispatcher.app;

import com.rivigo.finance.config.FinanceCommonsServiceConfig;
import com.rivigo.invoicedispatcher.config.DispatcherOAuth2ResourceConfig;
import com.rivigo.invoicedispatcher.config.PropertySourceConfig;
import com.rivigo.oauth2.resource.config.OAuth2ResourceConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.Arrays;

@ComponentScan({"com.rivigo.invoicedispatcher",
        "com.rivigo.finance",
        "com.rivigo.notification.client.service"})
@Configuration
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class})
@SpringBootApplication
@EnableAspectJAutoProxy
@EnableFeignClients(basePackages = {"com.rivigo.invoicedispatcher"})
@Slf4j
public class DispatcherInitializer extends SpringBootServletInitializer {
    public DispatcherInitializer() {
        super();
        setRegisterErrorPageFilter(false);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(
                DispatcherInitializer.class,
                OAuth2ResourceConfig.class,
                FinanceCommonsServiceConfig.class,
                PropertySourceConfig.class
        );
    }

    public static void main(String[] args) {
        Class[] sources = {
                DispatcherInitializer.class,
                DispatcherOAuth2ResourceConfig.class,
                FinanceCommonsServiceConfig.class,
                PropertySourceConfig.class
        };

        ConfigurableApplicationContext ctx = SpringApplication.run(sources, args);

        log.info("Beans provided by Spring Boot ");
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        Arrays.stream(beanNames).forEach(bean -> log.info(bean));
    }

}
