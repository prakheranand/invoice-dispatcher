package com.rivigo.invoicedispatcher.interfaces;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;

public interface ParentDispatchJobConsumer {
    //override this function for other consumers
    public void consumes(ConsumerRecord<?, ?> record) throws IOException;
}
