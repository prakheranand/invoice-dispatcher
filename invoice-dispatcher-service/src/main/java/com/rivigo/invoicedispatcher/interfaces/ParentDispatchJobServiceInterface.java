package com.rivigo.invoicedispatcher.interfaces;

import com.rivigo.invoicedispatcher.api.DispatchJobDto;
import com.rivigo.invoicedispatcher.api.ParentDispatcherJobDto;
import com.rivigo.invoicedispatcher.entity.mysql.DispatcherEntity;
import com.rivigo.invoicedispatcher.entity.mysql.InvoiceDispatcherEntity;

import java.util.List;

public interface ParentDispatchJobServiceInterface {
    public List<DispatcherEntity> createDispatchJobs(ParentDispatcherJobDto parentDispatcherJobDto);
}
