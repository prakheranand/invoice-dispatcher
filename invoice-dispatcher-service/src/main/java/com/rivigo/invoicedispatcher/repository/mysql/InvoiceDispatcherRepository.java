package com.rivigo.invoicedispatcher.repository.mysql;

import com.rivigo.invoicedispatcher.entity.mysql.InvoiceDispatcherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvoiceDispatcherRepository extends JpaRepository<InvoiceDispatcherEntity, Long> {
    @Query(
            "SELECT ide FROM InvoiceDispatcherEntity ide where ide.dispatchId = :DispatchId"  )
    List<InvoiceDispatcherEntity> findByDispatchId(@Param("DispatchId") Long dispatchId);
}
