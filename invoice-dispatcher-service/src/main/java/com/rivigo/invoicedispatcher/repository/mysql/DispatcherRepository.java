package com.rivigo.invoicedispatcher.repository.mysql;

import com.rivigo.invoicedispatcher.entity.mysql.DispatcherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DispatcherRepository extends JpaRepository<DispatcherEntity, Long> {

    @Query(value = "select de from DispatcherEntity de where de.status = :STATUS")
    List<DispatcherEntity> findByStatus(@Param("STATUS") String status);

}
