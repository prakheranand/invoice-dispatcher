package com.rivigo.invoicedispatcher.repository.mysql;

import com.rivigo.invoicedispatcher.entity.mysql.InvoicingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<InvoicingEntity, Long> {
    @Query(
            "SELECT ie FROM " +
                    "InvoicingEntity ie where ie.id = :InvoiceId "  )
    InvoicingEntity findByInvoiceId(@Param("InvoiceId") Long invId);

    @Query(
            "SELECT ie FROM " +
                    "InvoicingEntity ie where ie.id in ( :InvoiceId ) "  )
    List<InvoicingEntity> findByInvoiceIds(@Param("InvoiceId") List<Long> ids);

    @Query(
            "SELECT ie FROM " +
                    "InvoicingEntity ie where ie.invoiceNumber = :InvoiceNumber "  )
    InvoicingEntity findByInvoiceNumber(@Param("InvoiceNumber") String invNumber);

    @Query(
            "SELECT ie FROM " +
                    "InvoicingEntity ie where ie.invoiceNumber in ( :InvoiceNumbers ) "  )
    List<InvoicingEntity> findByInvoiceNumbers(@Param("InvoiceNumbers") List<String> numbers);

}
