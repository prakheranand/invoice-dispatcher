package com.rivigo.invoicedispatcher.api;

import lombok.Data;

import java.util.List;

@Data
public class ParentDispatcherJobDto {
    private List<String> invoiceNumbers;
    private String approvedBy;
    private long approvedAt;
}
