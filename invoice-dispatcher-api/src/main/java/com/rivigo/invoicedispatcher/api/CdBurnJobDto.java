package com.rivigo.invoicedispatcher.api;

import lombok.Data;

@Data
public class CdBurnJobDto {
    private int dispatchId;
    private String priority;
}
