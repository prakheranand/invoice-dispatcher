package com.rivigo.invoicedispatcher.api;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PrintJobDto {
    private Long dispatchId;
    private String priority;
}

