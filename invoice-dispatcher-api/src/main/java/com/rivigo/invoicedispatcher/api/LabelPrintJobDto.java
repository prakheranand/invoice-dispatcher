package com.rivigo.invoicedispatcher.api;

import lombok.Data;

@Data
public class LabelPrintJobDto {
    private Long dispatchId;
    private Label label;
    private String priority;

    @Data
    public class Label {
        String header;
        String attention;
        String addressLine1;
        String addressLine2;
        String landmark;
        String city;
        String state;
        String pincode;
        String contact;
        String footer;
    }


}
