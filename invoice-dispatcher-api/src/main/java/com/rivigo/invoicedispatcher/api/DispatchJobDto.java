package com.rivigo.invoicedispatcher.api;

import lombok.Data;

import java.util.List;
import java.util.Map;

//Need to send to print utils to download files
@Data
public class DispatchJobDto {
    private Long dispatchId;
    private String name;
    private List<String> invoiceNumbers;


//    private Map<DocumentType, List<String>> s3urls;
//    private String label;
//
//
//
//    public enum DocumentType {
//        pod,
//        pdf,
//    }

}
